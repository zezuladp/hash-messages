##Service with two endpoints.

/messages takes a message (a string) as a POST and returns the SHA256 hash digest of that message (in hexadecimal format)
/messages/<hash> is a GET request that returns the original message. A request to a non-existent <hash> returns a 404 error.

Note that if the server crashes, stored hashes do not persist. Easiy doable by linking datastore docker container such as redis. Or even just storing in a file.

###Example Queries

`curl -XPOST -k https://54.186.129.80/messages -H "Content-Type: application/json" -d '{"message": "bar"}'`
`curl -k https://54.186.129.80/messages/fcde2b2edba56bf408601fb721fe9b5c338d10ee429ea04fae5511b68fbf8fb9`

## Prerequisites
Have docker installed on your machine

##Building

`docker build . -t zezuladp2/hash-messages`

##Running

`docker run -d -t -p 0.0.0.0:443:5000 zezuladp2/hash-messages`

###Generating x509 cert

`openssl genrsa -aes256 -out server.key 2048`

`openssl req -new -key server.key -out server.csr`

`cp server.key server.key.org`

`openssl rsa -in server.key.org -out server.key`

`openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt`

###Log Rotation

runit's built in log management svlogd does log rotation automatically based on some defaults. These can be overriden in a service/log/config file