#!/usr/bin/python
#
# Maintainer Dan Zezula
# Jan 2017
#
#
from flask import Flask, request, abort
import json
# import sys
from OpenSSL import SSL
import hashlib
import logging

app = Flask(__name__)
HASHES = {}


# POST takes in JSON with message and returns the sha256 digest
@app.route('/messages', methods=['POST'])
def message():
    data = json.loads(request.data)
    digest = hashlib.sha256(data['message']).hexdigest()
    HASHES[digest] = data['message']
    logging.info("digest {}".format(digest))
    return json.dumps({"digest": digest})


# GET RESTful endpoint, hash as string. Returns original message
@app.route('/messages/<string:message>', methods=['GET'])
def get_msg(message):
    if message in HASHES:
        logging.info("message {}".format(message))
        return json.dumps({"message": HASHES[message]})
    else:
        abort(404)


# @app.route('/test-crash')
# def crash():
#    sys.exit(0)

if __name__ == '__main__':
    app.run(host="0.0.0.0", ssl_context=('server.crt', 'server.key'))
