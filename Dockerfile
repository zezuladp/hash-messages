FROM python:2
MAINTAINER Dan Zezula zezuladp@gmail.com

# Install service manager runit
RUN apt-get update
RUN apt-get install runit -y

# Set up service and register under sv
RUN mkdir /etc/sv/hash-messages
ADD service /etc/sv/hash-messages
RUN ln -s /etc/sv/hash-messages /etc/service/

# Add app code
ADD hash-messages /opt/

# Setup and install app
WORKDIR /opt/
RUN pip install -r requirements.txt

# Start runit
CMD ["runsvdir-start"]
